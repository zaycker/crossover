'use strict';

/**
 * @ngdoc function
 * @name crossoverApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the crossoverApp
 */
angular.module('crossoverApp')
  .controller('MainCtrl', ['$scope', 'deployList', function ($scope, deployList) {
    $scope.list = [];
    $scope.active = '';

    deployList.getList().then(function(list) {
      $scope.list = list;
    });

    $scope.onClick = function(activeId) {
      $scope.active = activeId;
    }
  }]);
