'use strict';

describe('Service: deployList', function () {

  // load the service's module
  beforeEach(module('crossoverApp'));

  // instantiate service
  var deployList;
  beforeEach(inject(function (_deployList_) {
    deployList = _deployList_;
  }));

  it('should do something', function () {
    expect(!!deployList).toBe(true);
  });

});
